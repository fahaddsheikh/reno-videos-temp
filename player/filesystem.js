window.onload = function() {
   butDir = document.getElementById('sv');
   banner = document.getElementById('banner');
   videoDiv = document.getElementById('videos');
   flagdisabled = document.getElementById('flagdisabled');
   novideos = document.getElementById('novideos');
   err = document.getElementById('error');

   

   if (typeof window.chooseFileSystemEntries === "function") { 
        flagdisabled.setAttribute("class", "hide");;
        let handle;
        butDir.addEventListener('click', async (e) => {
            var c = 0;
            const opts = {type: 'openDirectory'};
            handle = await window.chooseFileSystemEntries(opts);
            const entries = await handle.getEntries();
            const path = window.location.pathname;
            var directory = path.substring(path.indexOf('/'), path.lastIndexOf('/'));
            var is_empty = true;
            for await (const entry of entries) {
                is_empty = false;
                var file = entry.name;
                var split = file.split(".");
                var filename = "a" + encodeURI(split[0]);
                var extension = split[1];
                if (extension === 'mp4' || extension === 'webm' || extension === 'ogg' || extension === 'avi') {
                    var col = document.createElement("div");
                    col.setAttribute("class", "col");
                    var video = document.createElement("video");
                    video.setAttribute("class", "video-js");
                    video.setAttribute("id", filename.replace(/^[^a-z]+|[^\w:.-]+/gi, "") );
                    video.setAttribute("muted", "mute");
                    video.setAttribute("loop", "true");
                    video.setAttribute("preload", "auto");
                    video.setAttribute("controls", "true");
                    video.setAttribute("poster", "poster.jpg");
                    video.setAttribute("data-setup", "{}");
                    var src = document.createElement("source");
                    src.setAttribute("src", directory+"/"+handle.name+"/"+entry.name);
                    src.setAttribute("type", "video/"+extension);

                    video.appendChild(src);
                    col.appendChild(video);
                    videoDiv.appendChild(col);

                    var player = videojs(filename.replace(/^[^a-z]+|[^\w:.-]+/gi, ""));  

                    player.on('error', function(e) {
                        e.stopImmediatePropagation();
                        var error = this.player().error();
                        document.getElementById("err").innerHTML = error.message;
                        banner.setAttribute("class", "hide");
                        novideos.setAttribute("class", "show");
                        err.setAttribute("class", "show");
                    });                 
                }
            }
            if (is_empty) {
                novideos.setAttribute("class", "show")
                banner.setAttribute("class", "hide");
                butDir.setAttribute("class", "hide"); 
            } else {
                banner.setAttribute("class", "show");
                butDir.setAttribute("class", "hide"); 
            }
                      
        });
    } else {
        butDir.setAttribute("class", "hide");
        flagdisabled.setAttribute("class", "show");
    }
};