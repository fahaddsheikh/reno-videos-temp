const binds = [ 81, 87, 69, 82, 84, 89, 85, 73, 79, 80, 65, 83, 68, 70, 71, 72, 74, 75, 76, 90, 88, 67, 86, 66, 78, 77 ]
var n = 0;
videos = [];


videojs.hook('beforesetup', function(videoEl, options) {
	options.autoPlay = true;
	options.userActions = {
    	hotkeys: {
	      	playPauseKey: function(event) {
	        	return (event.which === 96);
	      	},
	      	muteKey: function(event) {
	        	return (event.which === 98);
	      	}
    	}
  	}
	return options;
});

videojs.hookOnce('setup', function(player) {
	player.focus();
});

videojs.hook('setup', function(player) {
	player.binds(binds[n]);
	player.fullscreen();
	n++;
});

videojs.registerPlugin('fullscreen', function(n) {
	this.on('play', function () {
		this.addClass('active');
		this.requestFullscreen();
        this.muted(false);
        this.hasStarted(true);

        var Players = videojs.getPlayers();

        for (var op in Players) {
        	plyr = videojs.getPlayer(op);
          	if (plyr.id_ != this.id_) {
          		plyr.pause();
          		plyr.currentTime(0);
          		plyr.exitFullWindow();
          		plyr.hasStarted(false);
          	}
        }
	});
	this.on('pause', function () {
		this.hasStarted(false);
	});
});

videojs.registerPlugin('binds', function(e) {

	var video_binds = {};
	video_binds.playKey = e;
	video_binds.videoId = this.id_;
	videos.push(video_binds);

});

document.addEventListener('keydown', logKey);

function logKey(e) {
	for (let elem in videos) {  
		
		element = videos[elem];
		playKey = parseInt( element.playKey );
		if (e.keyCode === playKey) {
			
			videoId = element.videoId;
			player = videojs.getPlayer(videoId);
			player.ready(function() {
				player.paused() ? player.play() : player.pause();
			});
		}
	}
	if (e.keyCode === 96) {
		player.paused() ? player.play() : player.pause();
	}
	if (e.keyCode === 98) {
		player.muted() ? player.muted(false) : player.muted(true);
	}
	if (e.keyCode === 8) {
		player.pause();
  		player.currentTime(0);
  		player.exitFullscreen();
  		player.exitFullWindow();
  		player.hasStarted(false);
	}
}


document.addEventListener("fullscreenchange", function(event){
    if(!document.fullscreenElement){
        for (let vid in videos) { 
        	video = videos[vid];
        	videoid = parseInt( video.videoId );
        	playr = videojs.getPlayer(video.videoId);
        	playr.pause();
        }
    } 
}, false);